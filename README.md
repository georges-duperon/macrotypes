- all languages from the paper are in implemented with both Racket syntax (in `macrotypes/examples/`) and Turnstile syntax (in `turnstile/examples/`)

- see `macrotypes/examples/README.md` for language reuse information

- tests are in `macrotypes/examples/tests/` and `turnstile/examples/tests/` directories

- run all tests (from test directory) with `racket run-all-tests.rkt`

- run just mlish tests (from test directory) with `racket run-all-mlish-tests.rkt`

- running tests require Racket v6.5 or later